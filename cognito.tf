variable "scopes" {
  default = {
    scope1 = {
      "scope_name": "read"
      "scope_description": "description"
    },
    scope2 = {
      "scope_name": "update"
      "scope_description": "description"
    }
  }
}

resource "aws_cognito_user_pool" "pool" {
  name = "SG Pool"
  alias_attributes = ["email"]
  schema {
    attribute_data_type = "String"
    name = "email"
    required = true
  }
}

resource "aws_cognito_user_pool_client" "client" {
  name = "SG Client"
  supported_identity_providers = ["COGNITO"]
  user_pool_id = aws_cognito_user_pool.pool.id
  allowed_oauth_flows =  ["code", "implicit"]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_scopes = ["email", "openid", "${ aws_cognito_resource_server.resource.name}/${ var.scopes.scope1.scope_name}", "${ aws_cognito_resource_server.resource.name}/${ var.scopes.scope2.scope_name}"]
  callback_urls = ["https://localhost"]
}

resource "aws_cognito_user_pool_domain" "main" {
  domain       = "sg-assessment"
  user_pool_id = aws_cognito_user_pool.pool.id
}

resource "aws_cognito_resource_server" "resource" {
  identifier = "graphics"
  name       = "graphics"

  dynamic "scope" {
    for_each = [for key, value in var.scopes: {
      scope_name     = value.scope_name
      scope_description     = value.scope_description
    }]

    content {
      scope_name     = scope.value.scope_name
      scope_description = scope.value.scope_description
    }
  }

  user_pool_id = aws_cognito_user_pool.pool.id
}

resource "aws_cognito_user_pool" "secretpool" {
  name = "SG Pool Secret"
  alias_attributes = ["email"]
  schema {
    attribute_data_type = "String"
    name = "email"
    required = true
  }
}

resource "aws_cognito_resource_server" "secretresource" {
  identifier = "graphics"
  name       = "graphics"

  dynamic "scope" {
    for_each = [for key, value in var.scopes: {
      scope_name     = value.scope_name
      scope_description     = value.scope_description
    }]

    content {
      scope_name     = scope.value.scope_name
      scope_description = scope.value.scope_description
    }
  }

  user_pool_id = aws_cognito_user_pool.secretpool.id
}

resource "aws_cognito_user_pool_domain" "secretdomain" {
  domain       = "sg-assessment-secret"
  user_pool_id = aws_cognito_user_pool.secretpool.id
}

resource "aws_cognito_user_pool_client" "secretclient" {
  name = "SG Client"
  supported_identity_providers = ["COGNITO"]
  user_pool_id = aws_cognito_user_pool.secretpool.id
  allowed_oauth_flows =  ["client_credentials"]
  allowed_oauth_flows_user_pool_client = true
  generate_secret = true
  allowed_oauth_scopes = ["${ aws_cognito_resource_server.secretresource.name}/${ var.scopes.scope1.scope_name}", "${ aws_cognito_resource_server.secretresource.name}/${ var.scopes.scope2.scope_name}"]
  callback_urls = ["https://localhost"]
}
