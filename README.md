# Speech Grapics Test - Terraform
This repository contains the terraform that will spin up the relevant resources in my personal AWS account. The services used were:

- Cognito
- DynamoDB
- API Gateway
- Lambda
- SSM
- S3
- Cloudwatch
- IAM

## Building, testing and deploying

This will deploy an infrastructure consisting of a REST API Gateway that will authorize users via Cognito, forwarding on to the relevant Lambda functions. Permissions of both functions are detailed below:
- Login will need to interact with SSM to fetch the Client ID and Secret, which are stored on creation of the user pool.
- Graphics will make the relevant calls to DynamoDB depending on the HTTP method.
- Both will will store the relevant logs to CloudWatch

State is stored in S3 and DynamoDB.

On deploy to master it will run `Validate`, `Plan` and `Apply`, with `Destroy` an optional/manual step

## Using the API
Once the apply has completed, it will output the url for the API deployed, **for example** : `https://p7z2w4crec.execute-api.eu-west-2.amazonaws.com/v1`

Below are cURL commands that can be imported into postman to test the following scenarios:

##### Login
This will return a bearer token to be used in calls to `/send`. The Client ID and Secret is stored in SSM by terraform on apply and is used by the lambda function. Details of the
function is found in the [Cognito Login](https://gitlab.com/speech-graphics-assessment/cognito-login) repo. This will utilise the Cognito User pool created.
```
curl -X POST \
  https://p7z2w4crec.execute-api.eu-west-2.amazonaws.com/v1/login \
  -H 'cache-control: no-cache'
```

##### Send (POST)
This will take a JSON object and update/add records in the DynamoDB table, detailed in the the [DynamoDB repo](https://gitlab.com/speech-graphics-assessment/dynamodb).

The route is protected by a Cognito Authorizer which will only give access with a valid Bearer token. The call above will return a token with both of its scopes: `graphics/read` & `graphics/update`,
however the check for this route is on the `graphics/update`.


```
curl -X POST \
  'https://p7z2w4crec.execute-api.eu-west-2.amazonaws.com/v1/send?TableName=graphicsDB' \
  -H 'Authorization: Bearer $TOKEN' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
	"TableName": "graphicsDB",
	"Item": {
		"id": "1",
		"value": "Hello World"
	}
}'
```

##### Send (GET)
This was deployed for test purposes to check that calls made above were being registered. Much like the above method, it is protected by the same Cognito Authorizer, but on the `graphics/read` scope.

```
curl -X GET \
  'https://p7z2w4crec.execute-api.eu-west-2.amazonaws.com/v1/send?TableName=graphicsDB' \
  -H 'Authorization: Bearer $TOKEN' \
  -H 'Cache-Control: no-cache' \
  -H 'cache-control: no-cache'
``` 


## Difficulties faced and improvements
User pools - As detailed in the [Cognito Login](https://gitlab.com/speech-graphics-assessment/cognito-login) repo, I was unsure on the Auth Flow to
follow for this, and through testing I had created two types, one with a Client Secret, and one that would require a username/password.

Domain name - I had my own registered domain in Route53, with an issued certificate in Certificate Manager, and was able to create the relevant hosted zone
and API mapping via terraform, so that in theory, `api.example.com/login` would always be the url for the API, however I was couldn't get past DNS issues that I was experiencing,
these issues were replicated when manually created too. 

Remote State - A pain point I encountered, but managed to fix, was storing the tfstate in an s3 bucket, with the locks going into a dynamoDB table. Initially, when I ran
`terraform apply` from my CI/CD pipeline, it was duplicating resources that were created when running locally. This required manual creation of the relevant resources 
outside of the terraform state so that they are not torn down on destroy.
