
resource "aws_api_gateway_rest_api" "api" {
name = "SG REST"
description = "Proxy to handle requests to our API"
}

resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "send"
}

resource "aws_api_gateway_method" "proxy" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.proxy.id
  http_method   = "GET"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.auth.id
  authorization_scopes = ["graphics/read"]
}

resource "aws_api_gateway_method" "update" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.proxy.id
  http_method   = "POST"
  authorization = "COGNITO_USER_POOLS"
  authorizer_id = aws_api_gateway_authorizer.auth.id
  authorization_scopes = ["graphics/update"]
}

resource "aws_api_gateway_integration" "lambda" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_method.proxy.resource_id
  http_method = aws_api_gateway_method.proxy.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.graphicsData.invoke_arn
}

resource "aws_api_gateway_integration" "lambdaUpdate" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_method.update.resource_id
  http_method = aws_api_gateway_method.update.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.graphicsData.invoke_arn
}

resource "aws_api_gateway_resource" "login" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "login"
}

resource "aws_api_gateway_method" "token" {
  rest_api_id   = aws_api_gateway_rest_api.api.id
  resource_id   = aws_api_gateway_resource.login.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "lambdaLogin" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_method.token.resource_id
  http_method = aws_api_gateway_method.token.http_method

  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.cognitoLogin.invoke_arn
}

resource "aws_api_gateway_method_response" "res" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.login.id
  http_method = aws_api_gateway_method.token.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
}

resource "aws_api_gateway_integration_response" "MyDemoIntegrationResponse" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  resource_id = aws_api_gateway_resource.login.id
  http_method = aws_api_gateway_method.token.http_method
  status_code = aws_api_gateway_method_response.res.status_code

  response_templates = {
    "application/json" = ""
  }

  depends_on = [
    aws_api_gateway_integration.lambdaLogin
  ]
}

resource "aws_api_gateway_deployment" "deploy" {
  depends_on = [
    aws_api_gateway_resource.proxy,
    aws_api_gateway_resource.login,
    aws_api_gateway_integration.lambda,
    aws_api_gateway_integration.lambdaUpdate,
    aws_api_gateway_integration.lambdaLogin,
    aws_api_gateway_method.proxy,
    aws_api_gateway_method.token,
    aws_api_gateway_authorizer.auth
  ]

  rest_api_id = aws_api_gateway_rest_api.api.id
  stage_name  = "v1"
}


resource "aws_api_gateway_authorizer" "auth" {
  name                   = "CognitoAuth"
  rest_api_id            = aws_api_gateway_rest_api.api.id
  type = "COGNITO_USER_POOLS"
  provider_arns = [aws_cognito_user_pool.secretpool.arn]

}

output "api-gateway-url" {
  value = aws_api_gateway_deployment.deploy.invoke_url
}
