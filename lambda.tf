data "aws_s3_bucket_object" "login_version" {
  bucket = "speech-graphics"
  key = "cognito/current_version.txt"
}

data "aws_s3_bucket_object" "send_version" {
  bucket = "speech-graphics"
  key = "dynamodb/current_version.txt"
}

data "aws_s3_bucket_object" "dynamodb_lambda_zip" {
  bucket  = "speech-graphics"
  key     = "dynamodb/v${data.aws_s3_bucket_object.send_version.body}/index.zip"
}

data "aws_s3_bucket_object" "cognito_lambda_zip" {
  bucket  = "speech-graphics"
  key     = "cognito/v${data.aws_s3_bucket_object.login_version.body}/index.zip"
}

resource "aws_iam_role_policy" "lambda_policy" {
  name = "lambda_policy"
  role = aws_iam_role.role_for_LDC.id
  policy = file("policies/policy.json")
}


resource "aws_iam_role" "role_for_LDC" {
  name = "graphics-role"
  assume_role_policy = file("policies/assume_role_policy.json")
}

resource "aws_lambda_function" "graphicsData" {

  function_name     = "graphics"
  s3_bucket         = data.aws_s3_bucket_object.dynamodb_lambda_zip.bucket
  s3_key            = data.aws_s3_bucket_object.dynamodb_lambda_zip.key
  publish           = true
  role              = aws_iam_role.role_for_LDC.arn
  handler           = "index.handler"
  runtime           = "nodejs12.x"
}

resource "aws_lambda_function" "cognitoLogin" {

  function_name     = "login"
  s3_bucket         = data.aws_s3_bucket_object.cognito_lambda_zip.bucket
  s3_key            = data.aws_s3_bucket_object.cognito_lambda_zip.key
  role              = aws_iam_role.role_for_LDC.arn
  handler           = "index.handler"
  runtime           = "nodejs12.x"
}

resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.graphicsData.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}
resource "aws_lambda_permission" "apigwLogin" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.cognitoLogin.function_name
  principal     = "apigateway.amazonaws.com"

  # The "/*/*" portion grants access from any method on any resource
  # within the API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*"
}
