resource "aws_ssm_parameter" "id" {
  name  = "ClientID"
  type  = "SecureString"
  value = aws_cognito_user_pool_client.secretclient.id
  overwrite = true
}

resource "aws_ssm_parameter" "secret" {
  name  = "ClientSecret"
  type  = "SecureString"
  value = aws_cognito_user_pool_client.secretclient.client_secret
  overwrite = true
}
