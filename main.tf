provider "aws" {
  region = "eu-west-2"
}

terraform {
  backend "s3" {
    bucket         = "graphics-remote-state-storage-s3"
    key            = "global/s3/terraform.tfstate"
    region         = "eu-west-2"
    dynamodb_table = "graphics-state-lock-dynamo"
    encrypt        = true
  }
}

